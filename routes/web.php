<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\SubtagController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\sizeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ImageUploadController;
use App\Http\Controllers\frontcontroller;










/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [frontcontroller::class, 'index']);


Auth::routes();



Route::get('/home', [HomeController::class, 'index'])->name('home');

//admin route
Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('posts', PostController::class);
    Route::resource('province', ProvinceController::class) ;
    Route::resource('area', AreaController::class) ;
    Route::resource('store', StoreController::class) ;
    Route::resource('tag', TagController::class) ;
    Route::resource('subtag', SubtagController::class) ;
    Route::resource('color', ColorController::class) ;
    Route::resource('size', sizeController::class) ;
    Route::resource('product', ProductController::class) ;

});

//front route
Route::get('/stores',[frontcontroller::class, 'store'])->name('store');
Route::get('/subtagshow/{id}',[frontcontroller::class, 'subtagshow'])->name('subtag'); // Show All subtags store
Route::get('/products',[frontcontroller::class, 'products'])->name('products');  // Show All Product
Route::get('/product_detail/{id}',[frontcontroller::class, 'product_detail'])->name('product_detail'); // show one product

//cart
Route::get('cart', [ProductController::class, 'cart'])->name('cart');
Route::get('add-to-cart/{id}', [ProductController::class, 'addToCart'])->name('add.to.cart');
Route::patch('update-cart', [ProductController::class, 'updatecart'])->name('update.cart');
Route::delete('remove-from-cart', [ProductController::class, 'removecart'])->name('remove.from.cart');
Route::get('product-checkout', [ProductController::class, 'productcheckout'])->name('product-checkout');


// Route::resource('/{page}', AdminController::class);

Route::get('/', function () {
    $provinces = App\Models\province::all();
    return view('indexss',['provinces' => $provinces]);
});

Route::get('getarea/{id}', function ($id) {
    $area = App\Models\area::where('province_id',$id)->get();
    return response()->json($area);
});


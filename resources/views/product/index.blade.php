@extends('layouts.master')
@section('title')
    product
@stop
@section('css')
    <!-- Internal Data table css -->
    <link href="{{ URL::asset('assets/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/plugins/datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/datatable/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/plugins/datatable/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/datatable/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif

            <!-- head -->
            <div class="breadcrumb-header justify-content-between">
                <div class="my-auto">
                    <div class="d-flex">
                        <h4 class="content-title mb-0 my-auto">Manger products</h4><span
                            class="text-muted mt-1 tx-13 mr-2 mb-0">/
                            products list</span>
                    </div>
                </div>
                <div class="d-flex my-xl-auto right-content">
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-info btn-icon ml-2"><i
                                class="mdi mdi-filter-variant"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-danger btn-icon ml-2"><i class="mdi mdi-star"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-warning  btn-icon ml-2"><i
                                class="mdi mdi-refresh"></i></button>
                    </div>
                    <div class="mb-3 mb-xl-0">
                        <div class="btn-group dropdown">
                            <button type="button" class="btn btn-primary">created at</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuDate"
                                data-x-placement="bottom-end">
                                @foreach ($products as $product)
                                    <a class="dropdown-item" href="#">{{ $product->created_at }}</a>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- head -->



            <div class="card">
                <div class="card-body">
                    <!-- row opened -->
                    <div class="row row-sm">
                        <!--div-->
                        <div class="col-xl-12">
                            <div class="card mg-b-20">

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example" class="table key-buttons text-md-nowrap">
                                            <thead>
                                                <tr>
                                                    <th class="border-bottom-0">#</th>
                                                    <th class="border-bottom-0"> name</th>
                                                    <th class="border-bottom-0"> subtag</th>
                                                    <th class="border-bottom-0"> price</th>
                                                    <th class="border-bottom-0"> description</th>
                                                    {{-- <th class="border-bottom-0"> images</th> --}}

                                                    <th class="border-bottom-0" style="text-align: center"> sizes</th>
                                                    <th class="border-bottom-0" style="text-align: center">colors</th>
                                                    <th class="border-bottom-0">Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>{{ $product->id }}</td>
                                                        <td> {{ $product->name }}</td>
                                                        @if (!is_null($product->subtag))
                                                            <td> {{ $product->subtag->name }}</td>
                                                        @endif
                                                        <td> {{ $product->price }}</td>

                                                        <td> {{ $product->description }}</td>
                                                        {{-- <td> <img style="width: 200px;height:100px"
                                                            src="{{ URL::asset('uploads/products/' . $product->images[0]) }}" alt="erorr"></td> --}}

                                                        <td>
                                                            <div class="container">
                                                                <div class="row">
                                                                    @foreach ($product->sizes as $size)
                                                                        <div class="col-lg-6">
                                                                            {{ $size->name }}
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="container">
                                                                <div class="row">
                                                                    @foreach ($product->colors as $color)
                                                                        <div class="col-lg-2">
                                                                            <div title="{{ $color->name }}" name="colors"
                                                                                style="
                                                                                        background-color: {{ $color->name }};
                                                                                            width: 10px;
                                                                                            height: 10px;
                                                                                            border: solid 0px rgb(0, 0, 0);
                                                                                            border-radius: 50%;
                                                                                            margin-top: 10px;
                                                                                            "> </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                        </td>


                                                        {{-- <td> <img style="width: 200px;height:100px"
                                                                src="{{ $product->photo }}" alt="erorr"></td> --}}
                                                        <td>

                                                            <a class="btn btn-success"
                                                                href="{{ route('product.show', $product->id) }}">Show</a>
                                                            @can('product-edit')
                                                                <a class="btn btn-primary"
                                                                    href="{{ route('product.edit', $product->id) }}">Edit</a>
                                                            @endcan
                                                            @can('product-delete')
                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['product.destroy', $product->id], 'style' => 'display:inline']) !!}
                                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                                {!! Form::close() !!}
                                                            @endcan

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/div-->
                    </div>
                    <!-- /row -->





                </div>
                <div class="card-header">
                    @can('product-create')
                        <span class="float-right">
                            <a class="btn btn-primary" href="{{ route('product.create') }}">Create product</a>
                        </span>
                    @endcan

                </div>

                {!! $products->links() !!}


            </div>


        </div>
    </div>
@endsection

@section('js')
    <!-- Internal Data tables -->
    <script src="{{ URL::asset('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/jquery.dataTables.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatable/js/responsive.bootstrap4.min.js') }}"></script>
    <!--Internal  Datatable js -->
    <script src="{{ URL::asset('assets/js/table-data.js') }}"></script>
@endsection

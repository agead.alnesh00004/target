@extends('layouts.master')
@section('title')
    Create product
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
@stop

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- head -->
            <div class="breadcrumb-header justify-content-between">
                <div class="my-auto">
                    <div class="d-flex">
                        <h4 class="content-title mb-0 my-auto">Manger product</h4><span
                            class="text-muted mt-1 tx-13 mr-2 mb-0">/
                            product create</span>
                    </div>
                </div>
                <div class="d-flex my-xl-auto right-content">
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-info btn-icon ml-2"><i
                                class="mdi mdi-filter-variant"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-danger btn-icon ml-2"><i class="mdi mdi-star"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-warning  btn-icon ml-2"><i
                                class="mdi mdi-refresh"></i></button>
                    </div>
                    <div class="mb-3 mb-xl-0">
                        <div class="btn-group dropdown">
                            <button type="button" class="btn btn-primary">14 Aug 2019</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuDate"
                                data-x-placement="bottom-end">
                                <a class="dropdown-item" href="#">2015</a>
                                <a class="dropdown-item" href="#">2016</a>
                                <a class="dropdown-item" href="#">2017</a>
                                <a class="dropdown-item" href="#">2018</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- head -->

            <div class="card">
                <div class="card-body">

                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <strong>{{ \Session::get('success') }}</strong>
                    </div>
                @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">

                            <input type="text" class="form-control" name="name" placeholder="Name"
                                value="{{ old('name') }}">
                        </div>

                        <div class="form-group">

                            <input type="text" class="form-control" name="price" placeholder="price"
                                value="{{ old('price') }}">
                        </div>

                        <div class="form-group">

                            <input type="text" class="form-control" name="description" placeholder="description"
                                value="{{ old('description') }}">
                        </div>

                        <div class="form-group">



                                <input class="form-control" type="file" id="images" name="images[]" multiple>
                        </div>


                        <div class="form-group" id="accordionExample">

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse_subtags" aria-expanded="false"
                                        aria-controls="collapse_subtags">
                                        subtags </button>
                                </h2>
                                <div id="collapse_subtags" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">

                                        <div class="container">
                                            <div class="row">
                                                @foreach ($subtags as $item)
                                                    <div class="col-lg-3">
                                                        <input type="radio" name="subtag_id" id="{{ $item->name }}"
                                                            value="{{ $item->id }}">
                                                        <label for="{{ $item->name }}"
                                                            style="color: tomato;">{{ $item->name }}</label>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>




                        <div class="form-group" id="accordionExample">

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse_colors" aria-expanded="false"
                                        aria-controls="collapse_colors">
                                        colors </button>
                                </h2>
                                <div id="collapse_colors" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">

                                        <input type="checkbox" id="select-all">

                                        <label for="select-all" style="color: blue;">select-all</label>


                                        <div class="container">
                                            <div class="row">
                                                @foreach ($colors as $item)
                                                    <div class="col-lg-3">
                                                        <input type="checkbox" name="colors[]" id="{{ $item->name }}"
                                                            value="{{ $item->id }}">
                                                        <label for="{{ $item->name }}"
                                                            style="color: tomato;">{{ $item->name }}</label>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group" id="accordionExample">

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse_size" aria-expanded="false" aria-controls="collapse_size">
                                        sizes </button>
                                </h2>
                                <div id="collapse_size" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">

                                        <input type="checkbox" id="select-all">

                                        <label for="select-all" style="color: blue;">select-all</label>


                                        <div class="container">
                                            <div class="row">
                                                @foreach ($sizes as $item)
                                                    <div class="col-lg-3">
                                                        <input type="checkbox" name="sizes[]" id="{{ $item->name }}"
                                                            value="{{ $item->id }}">
                                                        <label for="{{ $item->name }}"
                                                            style="color: tomato;">{{ $item->name }}</label>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="card-header">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class="btn btn-danger" href="{{ route('product.index') }}">Back</a>
                        </div>

                    </form>

                </div>



            </div>
        </div>
    </div>
    </div>
@endsection





@section('js')

    <script>
        document.getElementById('select-all').onclick = function() {
            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (var checkbox of checkboxes) {
                checkbox.checked = this.checked;
            }
        }
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>

@endsection

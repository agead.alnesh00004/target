@extends('layouts.master')
@section('title')
Edit User
@stop
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

					<div style="margin-bottom: 50px;margin-top: 50px">
						<div class="d-flex"><h4 class="content-title mb-0 my-auto">Manger Tag</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ Tag Edit</span></div>
					</div>


        <div class="card">


            <div class="card-body">
                {!! Form::model($tag, ['route' => ['tag.update', $tag->id], 'method'=>'PATCH']) !!}
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    </div>


                    <div class="card-header">
                        <button type="submit" class="btn btn-primary">Submit</button>
                            <a class="btn btn-danger" href="{{ route('tag.index') }}">Back</a>

                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

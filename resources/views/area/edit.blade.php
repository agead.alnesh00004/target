@extends('layouts.master')
@section('title')
    Edit area
@stop
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div style="margin-bottom: 50px;margin-top: 50px">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto">Manger area</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                        area Edit</span>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <form action="{{ route('area.update', $area->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">

                            <select name="province_id" id="province_id" class="form-control">
                                <option selected hidden="" disabled="" value="default">Select the status</option>
                                @foreach ($provinces as $province)
                                    <option value="{{ $province->id }}"
                                        {{ $province->id == $area->province_id ? 'selected' : '' }}>
                                        {{ $province->name }}</option>
                                @endforeach

                            </select>

                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="area"
                                value="{{ old('name', $area->name) }}">
                        </div>


                        <div class="card-header">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class="btn btn-danger" href="{{ route('area.index') }}">Back</a>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    <br><b><br><br><br><br><br><br>
@endsection

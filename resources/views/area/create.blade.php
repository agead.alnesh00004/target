@extends('layouts.master')
@section('title')
    Create area
@stop
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                        <!-- head -->
                        <div class="breadcrumb-header justify-content-between">
                            <div class="my-auto">
                                <div class="d-flex">
                                    <h4 class="content-title mb-0 my-auto">Manger area</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                                        area create</span>
                                </div>
                            </div>
                    
                        </div>
                        <!-- head -->

            <div class="card">
 <div class="card-body">
                <form action="{{ route('area.store') }}" method="POST">
                    @csrf
                    <div class="form-group">

                        <select name="province_id" id="province_id" class="form-control">
                            <option selected hidden="" disabled="" value="default">Select the status</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="area">
                    </div>


                    <div class="card-header">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a class="btn btn-danger" href="{{ route('area.index') }}">Back</a>
                    </div>
                </form>
                {{-- <div class="card-body">
                    {!! Form::open(['route' => 'area.store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        <strong>provinces:</strong>
                        {!! Form::select('provinces[]', $provinces, [], ['name' => 'province_name','class' => 'form-control']) !!}
                    </div>



                    <div class="form-group">
                        <strong>area name:</strong>
                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                    </div>



                    <div class="card-header">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a class="btn btn-danger" href="{{ route('area.index') }}">Back</a>





                    </div> <!-- row -->

                </div> --}}
            </div>

            {{-- {!! Form::close() !!} --}}

        </div>
    </div>
    </div>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@endsection

@extends('layouts.master')
@section('title')
Show subtag
@stop
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif


            <!-- breadcrumb -->
            <div class="breadcrumb-header justify-content-between">
                <div class="my-auto">
                    <div class="d-flex">
                        <h4 class="content-title mb-0 my-auto"> Manger subtag</h4><span
                            class="text-muted mt-1 tx-13 mr-2 mb-0">/ Show subtag</span>
                    </div>
                </div>
                <div class="d-flex my-xl-auto right-content">
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button subtag="button" class="btn btn-info btn-icon ml-2"><i
                                class="mdi mdi-filter-variant"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button subtag="button" class="btn btn-danger btn-icon ml-2"><i class="mdi mdi-star"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button subtag="button" class="btn btn-warning  btn-icon ml-2"><i
                                class="mdi mdi-refresh"></i></button>
                    </div>
                    <div class="mb-3 mb-xl-0">
                        <div class="btn-group dropdown">
                            <button subtag="button" class="btn btn-primary">14 Aug 2019</button>
                            <button subtag="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuDate"
                                data-x-placement="bottom-end">
                                <a class="dropdown-item" href="#">2015</a>
                                <a class="dropdown-item" href="#">2016</a>
                                <a class="dropdown-item" href="#">2017</a>
                                <a class="dropdown-item" href="#">2018</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb -->

            <div class="card">

                <div class="card-body">
                    <div class="lead">
                        <strong>Name:</strong>
                        {{ $subtag->name }}
                    </div>


                    <div class="card-header">
                        <span class="float-right">
                            <a class="btn btn-danger" href="{{ route('subtag.index') }}">Back</a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

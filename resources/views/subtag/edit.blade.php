@extends('layouts.master')
@section('title')
    Edit subtag
@stop
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div style="margin-bottom: 50px;margin-top: 50px">
                <div class="d-flex"><h4 class="content-title mb-0 my-auto">Manger subtag</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ subtag Edit</span></div>
            </div>

            <div class="card">
 <div class="card-body">
                <form action="{{ route('subtag.update',$subtag->id) }}" method="POST"  enctype="multipart/form-data" >
                    @csrf
                    @method('PUT')

                    <div class="form-group">

                        <select name="tag_id" id="tag_id" class="form-control"  >
                            <option selected hidden="" disabled="" value="default">Select the tag</option>
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id  }}" {{ $tag->id == $subtag->tag_id ? 'selected' : '' }}>{{ $tag->name }}</option>
                            @endforeach

                        </select>

                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="subtag" value="{{old('name', $subtag->name)}}">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Photo:</label>
                        <input type="file" class="form-control" name="photo">
                    </div>


                    <div class="card-header">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a class="btn btn-danger" href="{{ route('subtag.index') }}">Back</a>
                    </div>



                </form>


        </div>
    </div>
    </div>
    </div>
@endsection

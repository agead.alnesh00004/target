<!DOCTYPE html>
<html lang="zxx">


<!-- product-detail06:46-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TARGET</title>

    <meta name="keywords" content="Furniture, Decor, Interior">
    <meta name="description" content="Furnitica - Minimalist Furniture HTML Template">
    <meta name="author" content="tivatheme">


    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ URL::asset('libs/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ URL::asset('libs/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('libs/nivo-slider/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/nivo-slider/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/font-material/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/slider-range/css/jslider.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/owl-carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">


    <!-- Template CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/reponsive.css') }}">

</head>

<body id="">
    <header>
        <!-- header left mobie -->
        <div class="header-mobile d-md-none">
            <div class="mobile hidden-md-up text-xs-center d-flex align-items-center justify-content-around">

                <!-- menu left -->
                <div id="mobile_mainmenu" class="item-mobile-top">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>

                <!-- logo -->
                <div class="mobile-logo">
                    <a href="index-2.html">
                        <img class="logo-mobile img-fluid" src="img/home/logo-mobie.png" alt="Prestashop_Furnitica">
                    </a>
                </div>

                <!-- menu right -->
                <div class="mobile-menutop" data-target="#mobile-pagemenu">
                    <i class="zmdi zmdi-more"></i>
                </div>
            </div>

            <!-- search -->
            <div id="mobile_search" class="d-flex">
                <div id="mobile_search_content">
                    <form method="get" action="#">

                        <input type="text" name="s" value="" placeholder="Search">
                        <button type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
                <div class="desktop_cart">
                    <a class="dropdown-content"  href="{{ route('cart') }}">


                    <div class="blockcart block-cart cart-preview tiva-toggle">
                        <div class="header-cart tiva-toggle-btn">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </div>

                        </div>
                    </a>

                    </div>
                </div>
            </div>
        </div>

        <!-- header desktop -->
        <div class="header-top d-xs-none ">
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-sm-2 col-md-2 d-flex align-items-center">
                        <div id="logo">
                            <a href="index-2.html">
                                <H1>TARGET</H1>
                            </a>
                        </div>
                    </div>

                    <!-- menu -->
                    <div class="col-sm-5 col-md-5 align-items-center justify-content-center navbar-expand-md main-menu">
                        <div class="menu navbar collapse navbar-collapse">
                            <ul class="menu-top navbar-nav">
                                @foreach ($tags as $item)
                                    <li class="">
                                        <a href="#" class="parent"
                                            id="{{ $item->name }} value="{{ $item->id }}"
                                            name="tag_id">{{ $item->name }}</a>

                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <!-- search and acount -->
                    <div class="col-sm-5 col-md-5 d-flex align-items-center justify-content-end" id="search_widget">


                        <div id="block_myaccount_infos" class="hidden-sm-down dropdown">
                            <!-- acount  -->
                            <ul class="navbar-nav ml-auto">
                                <div class="myaccount-title">
                                    <a href="#acount" data-toggle="collapse" class="acount">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Account
                                        </span>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </a>
                                </div>

                                <div id="acount" class="collapse">
                                    <div class="account-list-content">

                                        <!-- Authentication Links -->
                                        @guest

                                            @if (Route::has('login'))
                                                <div>
                                                    <a class="login" href="{{ route('login') }}"
                                                        title="Log in to your customer account">
                                                        <i class="fa fa-sign-in"></i>
                                                        <span>Sign in</span>
                                                    </a>
                                                </div>
                                            @endif

                                            @if (Route::has('register'))
                                                <div>
                                                    <a class="register" href="{{ route('register') }}" rel="nofollow">

                                                        <i class="fa fa-user"></i>
                                                        <span>Register Account</span>
                                                    </a>
                                                </div>
                                            @endif
                                        @else
                                            <li class="nav-item dropdown">
                                                <div class="dropdown">
                                                    <a class=" dropdown-toggle" href="#" role="button"
                                                        id="dropdownMenuLink" data-bs-toggle="dropdown"
                                                        aria-expanded="false">
                                                        {{ Auth::user()->name }}
                                                    </a>



                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <li>
                                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                    document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>




                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                method="POST" class="d-none">
                                                                @csrf
                                                            </form>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </li>
                                        @endguest
                                    </div>
                                </div>

                            </ul>
                        </div>
                        @can('view-cart')

                        <div class="desktop_cart">
                            <a class="dropdown-content"  href="{{ route('cart') }}">


                              <div class="blockcart block-cart cart-preview tiva-toggle">
                                  <div class="header-cart tiva-toggle-btn">
                                  <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              </div>

                              </div>
                          </a>

                          </div>
                          @endcan
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- main content -->
    <div class="main-content">
        <div id="wrapper-site">
            <div id="content-wrapper">
                <div id="main">
                    <div class="page-home">
                        <!-- fix margin -->

                        <nav class="breadcrumb-bg">
                            <div class="container no-index">
                                <div class="breadcrumb">

                                </div>
                            </div>
                        </nav>
                        <!-- breadcrumb -->
                        <nav class="breadcrumb-bg" style="margin-top: 78px">
                            <div class="container no-index">
                                <div class="breadcrumb">
                                    <ol>
                                        <li>
                                            <a href="#">
                                                <span>Home</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>STORES</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>SUBTAGS</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>PRODUCTS</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>PRODUCT DETAIL</span>
                                            </a>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </nav>

                        <div class="container">
                            <div class="content">
                                <div class="row">


                                    <div class="col-sm-8 col-lg-12 col-md-9">
                                        <div class="main-product-detail">
                                            <br>
                                            <div class="product-single row">
                                                <div class="product-detail col-xs-12 col-md-5 col-sm-5">
                                                    <div class="page-content" id="content">
                                                        <div class="images-container">
                                                            <div class="js-qv-mask mask tab-content border">
                                                                @foreach ($product->images as $img)
                                                                    @if ($loop->index == 0)
                                                                        <div id="item{{ $loop->index + 1 }}"
                                                                            class="tab-pane fade active in show">
                                                                            <img style="width: 430px;height:500px"
                                                                                src="{{ URL::asset('uploads/products/' . $img) }}"
                                                                                alt="img">
                                                                        </div>
                                                                    @else
                                                                        <div id="item{{ $loop->index + 1 }}"
                                                                            class="tab-pane fade">
                                                                            <img style="width: 430px;height:500px"
                                                                                src="{{ URL::asset('uploads/products/' . $img) }}"alt="img">
                                                                        </div>
                                                                    @endif
                                                                @endforeach





                                                            </div>
                                                            <ul class="product-tab nav nav-tabs d-flex">

                                                                @foreach ($product->images as $img)
                                                                    @if ($loop->index == 0)
                                                                        <li class="active col">
                                                                            <a href="#item{{ $loop->index + 1 }}"
                                                                                data-toggle="tab" aria-expanded="true"
                                                                                class="active show">
                                                                                <img style="width: 100px;height:100px"
                                                                                    src="{{ URL::asset('uploads/products/' . $img) }}"
                                                                                    alt="img">
                                                                            </a>
                                                                        </li>
                                                                    @else
                                                                        <li class="col">
                                                                            <a href="#item{{ $loop->index + 1 }}"
                                                                                data-toggle="tab">
                                                                                <img style="width: 100px;height:100px"
                                                                                    src="{{ URL::asset('uploads/products/' . $img) }}"
                                                                                    alt="img">
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach



                                                            </ul>
                                                            {{-- <div class="modal fade" id="product-modal"
                                                                role="dialog">
                                                                <div class="modal-dialog">

                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <div class="modal-body">
                                                                                <div class="product-detail">
                                                                                    <div>
                                                                                        <div class="images-container">
                                                                                            <div
                                                                                                class="js-qv-mask mask tab-content">
                                                                                                <div id="modal-item1"
                                                                                                    class="tab-pane fade active in show">
                                                                                                    <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                        alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item2"
                                                                                                    class="tab-pane fade">
                                                                                                    <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                        alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item3"
                                                                                                    class="tab-pane fade">
                                                                                                    <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                        alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item4"
                                                                                                    class="tab-pane fade">
                                                                                                    <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                        alt="img">
                                                                                                </div>
                                                                                            </div>
                                                                                            <ul
                                                                                                class="product-tab nav nav-tabs">
                                                                                                <li class="active">
                                                                                                    <a href="#modal-item1"
                                                                                                        data-toggle="tab"
                                                                                                        class=" active show">
                                                                                                        <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                            alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item2"
                                                                                                        data-toggle="tab">
                                                                                                        <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                            alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item3"
                                                                                                        data-toggle="tab">
                                                                                                        <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                            alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item4"
                                                                                                        data-toggle="tab">
                                                                                                        <img src="{{ URL::asset('img/product/PD1.PNG') }}"
                                                                                                            alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-info col-xs-12 col-md-7 col-sm-7">
                                                    <div class="detail-description">
                                                        <div class="price-del">

                                                            <h4 class="">Name : {{ $product->name }}</h4>
                                                            <br>

                                                        </div>
                                                        <hr>
                                                        <div class="price-del">
                                                            <h4 class="">Price : {{ $product->price }}$</h4>
                                                            <br>

                                                        </div>


                                                        <hr>
                                                        <div class="">

                                                            <div class="container">
                                                                <div class="row">

                                                                    <h4 class="size">Colors :</h4>
                                                                    @foreach ($product->colors as $item)
                                                                        <div href="#" class=""
                                                                            title="{{ $item->name }}"
                                                                            style="border: solid 1px rgba(238, 236, 236, 0.493);border-radius: 50%;background-color:{{ $item->name }};width:20px;height:20px;margin-left:5px;cursor: pointer;">
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>



                                                        </div>

                                                        <hr>

                                                        <div class="option has-border d-lg-flex size-color">
                                                            <div class="size">
                                                                <h4 class="size">Size :</h4>
                                                                <select>
                                                                    <option value="">Choose your size</option>
                                                                    @foreach ($product->sizes as $item)
                                                                        <option value="{{ $item->id }}">
                                                                            {{ $item->name }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>


                                                        </div>
                                                        <hr>
                                                        <h4>Description</h4>
                                                        {{-- <p class="description">
                                                            <span style="color: rgb(0, 0, 0);">style :</span> Casual

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);">Color : </span>Navy Blue

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Pattern :</span> Plain

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Length :</span> Long

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Season : </span>Summer

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);">Type : </span>Tunic

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Details :
                                                            </span>Button, High Low

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Fit Type :
                                                            </span>Regular Fit

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Neckline :</span>
                                                            Collar

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);">Sleeve Length :</span>
                                                            Short Sleeve

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Sleeve Type :
                                                            </span>Batwing Sleeve

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Sheer :</span> No
                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Hem Shaped :</span>
                                                            Asymmetrical

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);">Placket
                                                                Type:</span>Pullovers

                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Material :
                                                            </span>Batwing
                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Composition :
                                                            </span>Collar
                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);"> Fabric : </span>100%
                                                            Polyester
                                                            <br>
                                                            <span style="color: rgb(0, 0, 0);">Care Instructions
                                                                :</span> Non-Stretch

                                                        </p> --}}
                                                        <p class="description">
                                                            {{ $product->description }}
                                                        </p>

                                                        <hr>

                                                        @can('add-to-cart')

                                                        <div class="has-border cart-area">
                                                            <div class="product-quantity">
                                                                <div class="qty">
                                                                    <div class="input-group">

                                                                        <span class="add">
                                                                            <button
                                                                                class="btn btn-primary add-to-cart add-item"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <i class="fa fa-shopping-cart"
                                                                                    aria-hidden="true"></i>
                                                                                <span>Add to cart</span>
                                                                            </button>

                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <p class="product-minimal-quantity">
                                                            </p>
                                                        </div>
                                                        @endcan


                                                        <div class="content">

                                                        </div>



                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- footer -->
                <footer class="footer-one">
                    <div class="inner-footer">
                        <div class="container">
                            <div class="footer-top col-lg-12 col-xs-12">
                                <div class=" row">
                                    <div class="nov-html col-lg-4 col-sm-12 col-xs-12">
                                        <div class="block">
                                            <div class="block-content">
                                                <p class="logo-footer">
                                                    <img src="img/home/logo.png" alt="img">
                                                </p>
                                                <p class="content-logo">Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit sed do eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim
                                                </p>
                                            </div>
                                        </div>
                                        <div class="block">
                                            <div class="block-content">
                                                <ul>
                                                    <li>
                                                        <a href="#">About Us</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Reasons to shop</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">What our customers say</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Meet the teaml</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Contact our buyers</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="block">
                                            <div class="block-content">
                                                <p class="img-payment ">
                                                    <img class="img-fluid" src="img/home/payment-footer.png"
                                                        alt="img">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nov-html col-lg-4 col-sm-6">
                                        <div class="block m-top">
                                            <div class="title-block">
                                                Contact Us
                                            </div>
                                            <div class="block-content">
                                                <div class="contact-us">
                                                    <div class="title-content">
                                                        <i class="fa fa-home" aria-hidden="true"></i>
                                                        <span>Address :</span>
                                                    </div>
                                                    <div class="content-contact address-contact">
                                                        <p>123 Suspendis matti, Visaosang Building VST District NY
                                                            Accums, North American
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="contact-us">
                                                    <div class="title-content">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        <span>Email :</span>
                                                    </div>
                                                    <div class="content-contact mail-contact">
                                                        <p>support@domain.com</p>
                                                    </div>
                                                </div>
                                                <div class="contact-us">
                                                    <div class="title-content">
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                        <span>Hotline :</span>
                                                    </div>
                                                    <div class="content-contact phone-contact">
                                                        <p>+0012-345-67890</p>
                                                    </div>
                                                </div>
                                                <div class="contact-us">
                                                    <div class="title-content">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <span>Opening Hours :</span>
                                                    </div>
                                                    <div class="content-contact hours-contact">
                                                        <p>Monday - Sunday / 08.00AM- 19.00</p>
                                                        <span>(Except Holidays)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tiva-modules col-lg-4 col-md-6">
                                        <div class="block m-top">
                                            <div class="block-content">
                                                <div class="title-block">Newsletter</div>
                                                <div class="sub-title">Sign up to our newsletter to get the latest
                                                    articles, lookbooks voucher codes direct
                                                    to your inbox
                                                </div>
                                                <div class="block-newsletter">
                                                    <form action="#" method="post">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="email"
                                                                value="" placeholder="Enter Your Email">
                                                            <span class="input-group-btn">
                                                                <button class="effect-btn btn btn-secondary "
                                                                    name="submitNewsletter" type="submit">
                                                                    <span>subscribe</span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <input type="hidden" name="action" value="0">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block m-top1">
                                            <div class="block-content">
                                                <div class="social-content">
                                                    <div class="title-block">
                                                        Follow us on
                                                    </div>
                                                    <div id="social-block">
                                                        <div class="social">
                                                            <ul class="list-inline mb-0 justify-content-end">
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-google"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block m-top1">
                                            <div class="block-content">
                                                <div class="payment-content">
                                                    <div class="title-block">
                                                        Payment accept
                                                    </div>
                                                    <div class="payment-image">
                                                        <img class="img-fluid" src="img/home/payment.png"
                                                            alt="img">
                                                    </div>
                                                </div>
                                                <!-- Popup newsletter -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tiva-copyright">
                        <div class="container">
                            <div class="row">
                                <div class="text-center col-lg-12 ">
                                    <span>
                                        <a target="_blank" href="https://www.templateshub.net">Templates Hub</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

                <!-- back top top -->
                <div class="back-to-top">
                    <a href="#">
                        <i class="fa fa-long-arrow-up"></i>
                    </a>
                </div>

                <!-- menu mobie left -->
                <div class="mobile-top-menu d-md-none">
                    <button type="button" class="close" aria-label="Close">
                        <i class="zmdi zmdi-close"></i>
                    </button>
                    <div id="desktop_verticalmenu" class="tiva-verticalmenu block" data-count_showmore="17">
                        <div class="box-content block-content">
                            <div class="verticalmenu" role="navigation">
                                <ul class="menu level1">
                                    <li class="item  parent">
                                        <a href="#" class="hasicon" title="SIDE TABLE">
                                            <img src="img/home/table-lamp.png" alt="img">SIDE TABLE</a>
                                        <span class="arrow collapsed" data-toggle="collapse"
                                            data-target="#SIDE-TABLE">
                                            <i class="zmdi zmdi-minus"></i>
                                            <i class="zmdi zmdi-plus"></i>
                                        </span>
                                        <div class="subCategory collapse" id="SIDE-TABLE" aria-expanded="true"
                                            role="status">
                                            <ul>
                                                <li class="item">
                                                    <a href="#" title="Aliquam lobortis">Aliquam lobortis</a>
                                                </li>
                                                <li class="item  parent-submenu">
                                                    <a href="#" title="Duis Reprehenderit">Duis
                                                        Reprehenderit</a>
                                                    <span class="arrow collapsed" data-toggle="collapse"
                                                        data-target="#sub-Category">
                                                        <i class="zmdi zmdi-minus"></i>
                                                        <i class="zmdi zmdi-plus"></i>
                                                    </span>
                                                    <div class="subCategory collapse" id="sub-Category"
                                                        aria-expanded="true" role="status">
                                                        <ul>
                                                            <li class="item">
                                                                <a href="#" title="Aliquam lobortis">Aliquam
                                                                    lobortis</a>
                                                            </li>
                                                            <li class="item">
                                                                <a href="#" title="Duis Reprehenderit">Duis
                                                                    Reprehenderit</a>
                                                            </li>
                                                            <li class="item">
                                                                <a href="#" title="Voluptate">Voluptate</a>
                                                            </li>
                                                            <li class="item">
                                                                <a href="#" title="Tongue Est">Tongue Est</a>
                                                            </li>
                                                            <li class="item">
                                                                <a href="#" title="Venison Andouille">Venison
                                                                    Andouille</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="item">
                                                    <a href="#" title="Voluptate">Voluptate</a>
                                                </li>
                                                <li class="item">
                                                    <a href="#" title="Tongue Est">Tongue Est</a>
                                                </li>
                                                <li class="item">
                                                    <a href="#" title="Venison Andouille">Venison Andouille</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="item  parent group">
                                        <a href="#" class="hasicon" title="FI">
                                            <img src="img/home/fireplace.png" alt="img">FIREPLACE
                                        </a>
                                        <span class="arrow collapsed" data-toggle="collapse" data-target="#fi">
                                            <i class="zmdi zmdi-minus"></i>
                                            <i class="zmdi zmdi-plus"></i>
                                        </span>
                                        <div class="subCategory collapse" id="fi" aria-expanded="true"
                                            role="status">
                                            <div class="item">
                                                <div class="menu-content">
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>DINNING ROOM</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">Toshiba</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Samsung</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>BATHROOM</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">Toshiba</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Samsung</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier Media</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Gee</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Digimart</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Vitivaa</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sanaky</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sunshine</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>LIVING ROOM</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">Media</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Gee</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Digimart</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Vitivaa</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sanaky</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sunshine</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Toshiba</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Samsung</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>BEDROOM</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>KITCHEN</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags d-flex d-xs-flex-inherit">
                                                        <div class="title">
                                                            <b>Blender</b>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#">LG</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Sharp</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Electrolux</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Hitachi</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Panasonic</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Mitsubishi Electric</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Daikin</a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#">Haier</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item group-category-img parent group">
                                        <a href="#" class="hasicon" title="TABLE LAMP">
                                            <img src="img/home/table-lamp.png" alt="img">TABLE LAMP</a>
                                        <span class="arrow collapsed" data-toggle="collapse"
                                            data-target="#table-lamp">
                                            <i class="zmdi zmdi-minus"></i>
                                            <i class="zmdi zmdi-plus"></i>
                                        </span>
                                        <div class="subCategory collapse" id="table-lamp" aria-expanded="true"
                                            role="status">
                                            <div class="item">
                                                <div class="menu-content">
                                                    <div class="col-xs-12">
                                                        <span class="menu-title">Coventry dining</span>
                                                        <ul>
                                                            <li>
                                                                <a href="#">Accessories</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Activewear</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">ASOS Basic Tops</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Bags &amp; Purses</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Beauty</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Coats &amp; Jackets</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Curve &amp; Plus Size</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="menu-title">Amara stools</span>
                                                        <ul>
                                                            <li>
                                                                <a href="#">Accessories</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Activewear</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">ASOS Basic Tops</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Bags &amp; Purses</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Beauty</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Coats &amp; Jackets</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Curve &amp; Plus Size</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="menu-title">Kingston dining</span>
                                                        <ul>
                                                            <li>
                                                                <a href="#">Accessories</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Activewear</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">ASOS Basic Tops</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Bags &amp; Purses</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Beauty</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Coats &amp; Jackets</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Curve &amp; Plus Size</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="menu-title">Ellinger dining</span>
                                                        <ul>
                                                            <li>
                                                                <a href="#">Accessories</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Activewear</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">ASOS Basic Tops</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Bags &amp; Purses</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Beauty</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Coats &amp; Jackets</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Curve &amp; Plus Size</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="OTTOMAN">
                                            <img src="img/home/ottoman.png" alt="img">OTTOMAN
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="ARMCHAIR">
                                            <img src="img/home/armchair.png" alt="img">ARMCHAIR
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="CUSHION">
                                            <img src="img/home/cushion.png" alt="img">CUSHION
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="COFFEE TABLE">
                                            <img src="img/home/coffee_table.png" alt="img">COFFEE TABLE</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="SHELF">
                                            <img src="img/home/shelf.png" alt="img">SHELF
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="SOFA">
                                            <img src="img/home/sofa.png" alt="img">SOFA
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="DRESSING TABLE">
                                            <img src="img/home/dressing.png" alt="img">DRESSING TABLE</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="WINDOWN CURTAIN">
                                            <img src="img/home/windown.png" alt="img">WINDOWN CURTAIN</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="CHANDELIER">
                                            <img src="img/home/chandelier.png" alt="img">CHANDELIER
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="CEILING FAN">
                                            <img src="img/home/ceiling_fan.png" alt="img">CEILING FAN</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="WARDROBE">
                                            <img src="img/home/wardrobe.png" alt="img">WARDROBE
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="FLOOR LAMP">
                                            <img src="img/home/floor_lamp.png" alt="img">FLOOR LAMP</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="VASE-FLOWER ">
                                            <img src="img/home/vase-flower.png" alt="img">VASE-FLOWER
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="BED">
                                            <img src="img/home/bed.png" alt="img">BED
                                        </a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="BED GIRL">
                                            <img src="img/home/bed.png" alt="img">BED GIRL</a>
                                    </li>
                                    <li class="item">
                                        <a href="#" class="hasicon" title="BED BOY">
                                            <img src="img/home/bed.png" alt="img">BED BOY</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- menu mobie right -->
                <div id="mobile-pagemenu" class="mobile-boxpage d-flex hidden-md-up active d-md-none">
                    <div class="content-boxpage col">
                        <div class="box-header d-flex justify-content-between align-items-center">
                            <div class="title-box">Menu</div>
                            <div class="close-box">Close</div>
                        </div>
                        <div class="box-content">
                            <nav>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div id="megamenu" class="clearfix">
                                    <ul class="menu level1">
                                        <li class="item home-page has-sub">
                                            <span class="arrow collapsed" data-toggle="collapse" data-target="#home1"
                                                aria-expanded="true" role="status">
                                                <i class="zmdi zmdi-minus"></i>
                                                <i class="zmdi zmdi-plus"></i>
                                            </span>
                                            <a href="index-2.html" title="Home">
                                                <i class="fa fa-home" aria-hidden="true"></i>Home</a>
                                            <div class="subCategory collapse" id="home1" aria-expanded="true"
                                                role="status">
                                                <ul>
                                                    <li class="item">
                                                        <a href="index-2.html" title="Home Page 1">Home Page 1</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="home2.html" title="Home Page 2">Home Page 2</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="home3.html" title="Home Page 3">Home Page 3</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="home4.html" title="Home Page 4">Home Page 4</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="home5.html" title="Home Page 5">Home Page 5</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item has-sub">
                                            <span class="arrow collapsed" data-toggle="collapse" data-target="#blog"
                                                aria-expanded="false" role="status">
                                                <i class="zmdi zmdi-minus"></i>
                                                <i class="zmdi zmdi-plus"></i>
                                            </span>
                                            <a href="#" title="Blog">
                                                <i class="fa fa-address-book" aria-hidden="true"></i>Blog</a>

                                            <div class="subCategory collapse" id="blog" aria-expanded="true"
                                                role="status">
                                                <ul>
                                                    <li class="item">
                                                        <a href="blog-list-sidebar-left.html"
                                                            title="Blog List (Sidebar Left)">Blog List (Sidebar
                                                            Left)</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="blog-list-sidebar-left2.html"
                                                            title="Blog List (Sidebar Left) 2">Blog List (Sidebar
                                                            Left) 2</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="blog-list-sidebar-right.html"
                                                            title="Category Blog (Right column)">Blog List (Sidebar
                                                            Right)</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="blog-list-no-sidebar.html"
                                                            title="Blog List (No Sidebar)">Blog List (No Sidebar)</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="blog-grid-no-sidebar.html"
                                                            title="Blog Grid (No Sidebar)">Blog Grid (No Sidebar)</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="blog-detail.html" title="Blog Detail">Blog
                                                            Detail</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item group has-sub">
                                            <span class="arrow collapsed" data-toggle="collapse" data-target="#page"
                                                aria-expanded="false" role="status">
                                                <i class="zmdi zmdi-minus"></i>
                                                <i class="zmdi zmdi-plus"></i>
                                            </span>
                                            <a href="#" title="Page">
                                                <i class="fa fa-file-text-o" aria-hidden="true"></i>page</a>
                                            <div class="subCategory collapse" id="page" aria-expanded="true"
                                                role="status">
                                                <ul class="group-page">
                                                    <li class="item container group">
                                                        <div>
                                                            <ul>
                                                                <li class="item col-md-4 ">
                                                                    <span class="menu-title">Category Style</span>
                                                                    <div class="menu-content">
                                                                        <ul class="col">
                                                                            <li>
                                                                                <a
                                                                                    href="product-grid-sidebar-left.html">Product
                                                                                    Grid (Sidebar Left)</a>
                                                                            </li>
                                                                            <li>
                                                                                <a
                                                                                    href="product-grid-sidebar-right.html">Product
                                                                                    Grid (Sidebar Right)</a>
                                                                            </li>
                                                                            <li>
                                                                                <a
                                                                                    href="product-list-sidebar-left.html">Product
                                                                                    List (Sidebar Left) </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="item col-md-4 html">
                                                                    <span class="menu-title">Product Detail
                                                                        Style</span>
                                                                    <div class="menu-content">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="product-detail.html">Product
                                                                                    Detail (Sidebar Left)</a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">Product Detail
                                                                                    (Sidebar Right)</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="item col-md-4 html">
                                                                    <span class="menu-title">Bonus Page</span>
                                                                    <div class="menu-content">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="404.html">404 Page</a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="about-us.html">About Us
                                                                                    Page</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item has-sub">
                                            <a href="contact.html" title="Contact us">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>Contact us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Vendor JS -->
                <script src="{{ URL::asset('libs/jquery/jquery.min.js') }}"></script>
                <script src="{{ URL::asset('libs/popper/popper.min.js') }}"></script>
                <script src="{{ URL::asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
                <script src="{{ URL::asset('libs/nivo-slider/js/jquery.nivo.slider.js') }}"></script>
                <script src="{{ URL::asset('libs/owl-carousel/owl.carousel.min.js') }}"></script>
                <script src="{{ URL::asset('libs/slider-range/js/tmpl.js') }}"></script>
                <script src="{{ URL::asset('libs/slider-range/js/jquery.dependClass-0.1.js') }}"></script>
                <script src="{{ URL::asset('libs/slider-range/js/draggable-0.1.js') }}"></script>
                <script src="{{ URL::asset('libs/slider-range/js/jquery.slider.js') }}"></script>

                <!-- Template JS -->
                <script src="{{ URL::asset('js/theme.js') }}"></script>
</body>


<!-- product-detail07:06-->

</html>

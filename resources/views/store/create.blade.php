@extends('layouts.master')
@section('title')
    Create store
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
@stop

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- head -->
            <div class="breadcrumb-header justify-content-between">
                <div class="my-auto">
                    <div class="d-flex">
                        <h4 class="content-title mb-0 my-auto">Manger store</h4><span
                            class="text-muted mt-1 tx-13 mr-2 mb-0">/
                            store create</span>
                    </div>
                </div>
                <div class="d-flex my-xl-auto right-content">
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-info btn-icon ml-2"><i
                                class="mdi mdi-filter-variant"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-danger btn-icon ml-2"><i class="mdi mdi-star"></i></button>
                    </div>
                    <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-warning  btn-icon ml-2"><i
                                class="mdi mdi-refresh"></i></button>
                    </div>
                    <div class="mb-3 mb-xl-0">
                        <div class="btn-group dropdown">
                            <button type="button" class="btn btn-primary">14 Aug 2019</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuDate"
                                data-x-placement="bottom-end">
                                <a class="dropdown-item" href="#">2015</a>
                                <a class="dropdown-item" href="#">2016</a>
                                <a class="dropdown-item" href="#">2017</a>
                                <a class="dropdown-item" href="#">2018</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- head -->

            <div class="card">
                <div class="card-body">
                    <form action="{{ route('store.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">

                            <select name="area_id" id="area_id" class="form-control">
                                <option selected hidden="" disabled="" value="default">Select the city</option>
                                @foreach ($areas as $area)
                                    <option value="{{ $area->id }}">{{ $area->name }}</option>
                                @endforeach
                            </select>

                        </div>



                        <div class="form-group">

                            <select name="user_id" id="user_id" class="form-control">
                                <option selected hidden="" disabled="" value="default">Select the user</option>
                                @foreach ($users as $user)
                                @if (is_null($user->store)&&$user->hasRole('store'))


                                <option value="{{ $user->id }}">{{ $user->name }}</option>



                                @endif
                                @endforeach
                            </select>

                        </div>


                        <div class="form-group" id="accordionExample">

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Tags </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">

                                        <input type="checkbox"   id="select-all">

                                        <label for="select-all" style="color: blue;">select-all</label>


                                        <div class="container">
                                            <div class="row">
                                                @foreach ($tags as $item)
                                                    <div class="col-lg-3">
                                                        <input type="checkbox" name="tags[]" id="{{ $item->name }}"
                                                            value="{{ $item->id }}">
                                                        <label for="{{ $item->name }}"
                                                            style="color: tomato;">{{ $item->name }}</label>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">

                            <input type="text" class="form-control" name="name" placeholder="Name"
                                value="{{ old('name') }}">
                        </div>

                        <div class="form-group">

                            <input type="text" class="form-control" name="address" placeholder="Address"
                                value="{{ old('address') }}">
                        </div>

                        <div class="form-group">

                            <input type="text" class="form-control" name="phone" placeholder="phone"
                                value="{{ old('phone') }}">
                        </div>

                        <div class="form-group ml-100">

                            <div class="form-group">
                                <input name="photo" class="form-control" type="file" id="formFile"
                                    value="{{ old('photo') }}">
                            </div>
                        </div>

                        <div class="card-header">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class="btn btn-danger" href="{{ route('store.index') }}">Back</a>
                        </div>

                    </form>

                </div>



            </div>
        </div>
    </div>
    </div>
@endsection





@section('js')

    <script>
 document.getElementById('select-all').onclick = function() {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var checkbox of checkboxes) {
        checkbox.checked = this.checked;
    }
}
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>

@endsection

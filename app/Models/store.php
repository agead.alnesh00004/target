<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Multitenantable;

class store extends Model
{
    use HasFactory ;

    protected $fillable = ['name','address','phone','photo','area_id','user_id'];




public function area()
{
    return $this->belongsTo(area::class);
}


public function user()
{
    return $this->belongsTo(user::class);
}

public function tags()
{
    return $this->belongsToMany(tag::class,'store_tags');
}

public function products()
{
    return $this->hasMany(product::class);
}


public function getfeaturedAttribute($photo)
{
    return asset($photo);// مشان اقدر حط الصورة بس هيك $item->phto      بدل هي=> {{URL::asset($item->photo) }}
}


}

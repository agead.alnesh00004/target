<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Multitenantable;


class product extends Model
{
    use HasFactory;
    protected $fillable =
    ['name'
    ,'price'
    ,'description'
    ,'store_id'
    ,'subtag_id'
    ,'images'
    ,'created_by_user_id'];



    public function user()
    {
        return $this->belongsTo(User::class,'created_by_user_id');
    }


    public function store()
    {
        return $this->belongsTo(store::class);
    }



    public function subtag()
    {
        return $this->belongsTo(subtag::class);
    }


    public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }



    public function sizes()
    {
        return $this->belongsToMany(size::class,'products_sizes');
    }


    public function colors()
    {
        return $this->belongsToMany(color::class,'colors_products');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
    use HasFactory;

    protected $fillable = ['id','name'];



    public function stores()
    {
        return $this->belongsToMany(store::class,'store_tags');
    }


   public function subtags()
   {
       return $this->hasMany(subtag::class);
   }



}

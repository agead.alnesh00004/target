<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subtag extends Model
{
    use HasFactory;


    protected $fillable = ['name', 'tag_id', 'photo'];


    public function tag()
    {
        return $this->belongsTo(tag::class);
    }

    public function products()
    {
        return $this->hasMany(product::class);
    }

    public function getfeaturedAttribute($photo)
    {
        return asset($photo);
        // مشان اقدر حط الصورة بس هيك $item->phto      بدل هي=> {{URL::asset($item->photo) }}
    }
}

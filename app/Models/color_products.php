<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class color_products extends Model
{
    use HasFactory;


    protected $table='colors_products';
    protected $fillable = ['product_id','color_id'];

}

<?php

namespace App\Http\Controllers;

use App\Models\subtag;
use App\Models\tag;
use App\Http\Requests\StoresubtagRequest;
use App\Http\Requests\UpdatesubtagRequest;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class subtagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $subtags = subtag::orderBy('id')->paginate(5);

        return view('subtag.index', compact('subtags'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = tag::all();

        if($tags->count() == 0)
        {
        return redirect()->route('tag.create');
        }

        return view('subtag.create')
        ->with('tags', $tags);
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:subtags',
            'tag_id' => 'required',
            'photo' => 'required|image'
        ]);

        $photo = $request->photo;
        $newphoto = time().$photo->getClientOriginalName();
        //لدالة لوقت بتجيب وقت اضافة الصورة
        //والدالة التانية بتفصل الاسم عن الامتداد وبلزق الاسم مع الوقت
        $photo->move('uploads/subtags', $newphoto);





            $subtag=subtag::create($request->except('photo') + [
                'photo' => 'uploads/subtags/' . $newphoto
                             ]);


        return redirect()
        ->route('subtag.index')
        ->with('success', 'add subtag  successfully');
    }


    public function show(subtag $subtag)
    {
        $subtag = subtag::find($id);

        return view('subtag.show', compact('subtag'));
    }


    public function edit($id)
    {

        $tags = tag::all();

        if($tags->count() == 0)
        {
        return redirect()->route('tag.create');
        }

        $subtag = subtag::find($id);
        return view('subtag.edit')
        ->with('tags', $tags)
        ->with('subtag', $subtag);

    }


    public function update(Request $request, $id)
    {


        $subtag = subtag::find($id);
        $this->validate($request, [
            'name' => 'required|unique:subtags',
            'tag_id' => 'required',
        ]);
        //dd($request->all());
        //dd('ss');
        if ($request->has('photo')) {
            $photo = $request->photo;
            $newphoto = time() . $photo->getClientOriginalName();
            // عم يعطيني اسم الصورة بدون امتداد وعم يدمجها مع الوقت مشان مايصير في تكرار
            $photo->move('uploads/subtags', $newphoto);
            $subtag->photo = 'uploads/subtags/' . $newphoto;
        }

        $subtag->name = $request->name;
        $subtag->tag_id = $request->tag_id;

        $subtag->save();

        return redirect()
        ->route('subtag.index')
        ->with('success', 'edit subtag  successfully');

    }


    public function destroy($id)
    {
        subtag::find($id)->delete();

        return redirect()->route('subtag.index')
            ->with('success', 'subtag deleted successfully');
    }
}

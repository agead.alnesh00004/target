<?php

namespace App\Http\Controllers;

use App\Models\province;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;


class ProvinceController extends Controller
{



    public function index()
    {

        $provinces = province::orderBy('id')->paginate(5);

        return view('province.index', compact('provinces'));
    }


    public function create()

    {

        $provinces = province::all();
        return view('province.create')->with('provinces', $provinces);
    }



    public function destroy($id)
    {
        province::find($id)->delete();

        return redirect()->route('province.index')
            ->with('success', 'province deleted successfully');
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:provinces'
        ]);


        province::create($request->all());

        return redirect()
        ->route('province.index')
        ->with('success', 'province create successfully.');
    }


    public function show($id)
    {
        $province = province::find($id);

        return view('province.show', compact('province'));
    }



    public function edit($id)
    {
        $province = province::find($id);
        return view('province.edit', compact('province'));
    }


    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $province = province::find($id);
        $province->name = $request->input('name');
        $province->save();

        return redirect()->route('province.index')
            ->with('success', 'province updated successfully.');
    }





}

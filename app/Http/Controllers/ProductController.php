<?php

namespace App\Http\Controllers;

use App\Models\product;
use App\Models\subtag;
use App\Models\color;
use App\Models\size;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = product::where('created_by_user_id',Auth::id())->orderBy('id')->paginate(5);
        return view('product.index', compact('products'));

        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subtags = subtag::all();
        $colors = color::all();
        $sizes = size::all();

      if($subtags->count() == 0)
    {
return redirect()->route('subtag.create');
    }

if($colors->count() == 0)
    {
return redirect()->route('color.create');
    }

        return view('product.create')
        ->with('subtags', $subtags)
        ->with('colors', $colors)
        ->with('sizes', $sizes);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'name'=>'required',
            'price'=>'required',
            'description'=>'required',
            'subtag_id'=>'required',
            'colors'=>'required',
            'sizes'=>'required',
            'images'=>'required',

            ]);

            $image_names = [];
            // loop through images and save to /uploads directory
            foreach ($request->file('images') as $image) {
                $name = $image->getClientOriginalName();
                $image->move('uploads/products', $name);
                $image_names[] = $name;
            }


            // $product=product::create($request->all([
                //    'name'=>$request->name
                //    ,'price'=>$request->price
                //    ,'description'=>$request->description
                //    ,'store_id'=>Auth::user()->store->id
                //    ,'subtag_id'=>$request->subtag_id,

                // ]));
            $product = new product();
            $product->name = $request->name;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->store_id = Auth::user()->store->id;
            $product->subtag_id = $request->subtag_id;
            $product->images = json_encode($image_names);
            $product->created_by_user_id=Auth::id();

            $product->save();

            // $product=product::create($request->except('store_id') + [
            //     'store_id'=>Auth::user()->store->id
            //  ]);

            $product->colors()->attach($request->colors);
            $product->sizes()->attach($request->sizes);


            return redirect()
            ->route('product.index')
            ->with('success', 'product create successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $product = product::find($id);



        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtags = subtag::all();
        $colors = color::all();
        $sizes = size::all();

      if($subtags->count() == 0)
    {
return redirect()->route('subtag.create');
    }

if($colors->count() == 0)
    {
return redirect()->route('color.create');
    }

    $product = product::find($id);

        return view('product.edit')
        ->with('product', $product)
        ->with('subtags', $subtags)
        ->with('colors', $colors)
        ->with('sizes', $sizes);

     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $product = product::find($id);
        $this->validate($request, [
            'name'=>'required',
            'price'=>'required',
            'description'=>'required',
            'subtag_id'=>'required',
            'colors'=>'required',
            'sizes'=>'required',

        ]);
        //dd($request->all());

        // if ($request->has('photo')) {
        //     $photo = $request->photo;
        //     $newphoto = time() . $photo->getClientOriginalName();
        //     // عم يعطيني اسم الصورة بدون امتداد وعم يدمجها مع الوقت مشان مايصير في تكرار
        //     $photo->move('uploads/stores', $newphoto);
        // }

        $product->update($request->except('store_id') + [
            'store_id'=>Auth::user()->store->id
                         ]);

        // $store->save();


        $product->colors()->sync($request->colors); // مشان يعدل كل colors ويخزنها
        $product->sizes()->sync($request->sizes); // مشان يعدل كل colors ويخزنها

        return redirect()
        ->route('product.index')
        ->with('success', 'edit product  successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

            product::find($id)->delete();
            return redirect()
            ->route('product.index')
            ->with('success', 'product delete successfully.');

    }

    public function cart()
    {
        return view('cart');
    }
    public function addToCart($id)
    {
        $product = Product::findOrFail($id);

        $cart = session()->get('cart', []);

        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
        } else {
            $cart[$id] = [
                "name" => $product->name,
                "quantity" => 1,
                "price" => $product->price,
                "image" => $product->image
            ];
        }

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }



    // public function updatecart(Request $request)
    // {
    //     if($request->id && $request->quantity){
    //         $cart = session()->get('cart');
    //         $cart[$request->id]["quantity"] = $request->quantity;
    //         session()->put('cart', $cart);
    //         session()->flash('success', 'Cart updated successfully');
    //     }
    // }



    // public function removecart(Request $request)
    // {
    //     if($request->id) {
    //         $cart = session()->get('cart');
    //         if(isset($cart[$request->id])) {
    //             unset($cart[$request->id]);
    //             session()->put('cart', $cart);
    //         }
    //         session()->flash('success', 'Product removed successfully');
    //     }
    // }


    public function productcheckout()
    {

        return view('product-checkout');

        }

}

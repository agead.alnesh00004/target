<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\province;
use App\Models\area;
use App\Models\tag;
use App\Models\store;
use App\Models\subtag;
use App\Models\product;
use App\Models\color;
use App\Models\size;





class frontcontroller extends Controller
{
    public function index()
    {

        $provinces = province::all();
        $areas = area::all();


        return view('indexss')->with('provinces', $provinces)->with('areas', $areas);
    }

    public function store(Request $request)
{

    // dd($request->area);
if($request->tag_id){

    $tag=Tag::find($request->tag_id);

    $stores = $tag->stores()->where('area_id' ,$request->area)->get();


}else{
    $stores = store::where(['area_id' => $request->area])->get();

}
    $tags = tag::all();


    return view('store')
    ->with('tags', $tags)
    ->with('stores', $stores)
    ->with('area',$request->area);
}


public function subtagshow($id,Request $request)
{

  //  dd($request->tag_id);
    $store= store::find($id);

   // dd($store->tags){}
   if($request->tag_id){

       $selectedTag=$store->tags->where('id',$request->tag_id);
   }else{
    $selectedTag=$store->tags;

   }

//dd($selectedTag);


    return view('subtagshow')
    ->with('store', $store)->with('selectedTag',$selectedTag);

}


public function products(Request $request)
{

    // subtag_id    store_id
    // $store=store::find($request->store_id);



    $tags = tag::all();
    $products = product::where('subtag_id',$request->subtag_id)
    ->where('store_id',$request->store_id)
    ->get();
  //   dd($products);
    $colors = color::all();
    $sizes = size::all();
    $subtags = subtag::all();



    foreach($products as $product => $value)
    {
        $value->images=json_decode($value->images);
    }




    return view('products')
    ->with('tags', $tags)
    ->with('colors', $colors)
    ->with('sizes', $sizes)
    ->with('products', $products)
    ->with('tags', $tags)
    ->with('subtags', $subtags);
}

public function product_detail($id)
{
    $product= product::find($id);

    $product->images=json_decode($product->images);


    $tags = tag::all();
    $subtags = subtag::all();


    return view('product_detail')
    ->with('product', $product)
    ->with('tags', $tags)
    ->with('subtags', $subtags);


}





}




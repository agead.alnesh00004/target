<?php

namespace App\Http\Controllers;

use App\Models\area;
use App\Models\store;
use App\Models\tag;
use App\Models\User;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use session;

use Illuminate\Auth\Events\Validated;

class StoreController extends Controller
{

    public function index()
    {

        $stores = store::orderBy('id')->paginate(5);
        return view('store.index', compact('stores'));
    }

    public function create()

    {

        $areas = area::all();
        $tags = tag::all();
        $users = User::all();


      if($tags->count() == 0)
    {
return redirect()->route('tag.create');
    }

if($areas->count() == 0)
    {
return redirect()->route('area.create');
    }



        return view('store.create')
        ->with('areas', $areas)
        ->with('tags', $tags)
        ->with('users', $users);

    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'area_id' => 'required',
            'tags' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'photo' => 'required|image'
        ]);

        $photo = $request->photo;
        $newphoto = time().$photo->getClientOriginalName();
        //لدالة لوقت بتجيب وقت اضافة الصورة
        //والدالة التانية بتفصل الاسم عن الامتداد وبلزق الاسم مع الوقت
        $photo->move('uploads/stores', $newphoto);




        $store=store::create($request->except('photo') + [
            'photo' => 'uploads/stores/' . $newphoto
                         ]);

        $store->tags()->attach($request->tags);

        return redirect()
        ->route('store.index')
        ->with('success', 'Store create successfully.');
    }


    public function show($id)
    {
        $store = store::find($id);

        return view('store.show', compact('store'));
    }


    public function edit($id)
    {
        $users = User::all();
        $tags = tag::all();
        $areas = area::all();


      if($tags->count() == 0)
    {
return redirect()->route('tag.create');
    }

if($areas->count() == 0)
    {
return redirect()->route('area.create');
    }

        $store = store::find($id);

        return view('store.edit')
        ->with('store', $store)
        ->with('areas', $areas)
        ->with('tags', $tags)
        ->with('users', $users);
    }

    public function update(Request $request,$id)
    {
        $store = store::find($id);
        $this->validate($request, [
            'name' => 'required',
            'area_id' => 'required',
            'tags' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'photo' => 'required|image'
        ]);
        //dd($request->all());

        if ($request->has('photo')) {
            $photo = $request->photo;
            $newphoto = time() . $photo->getClientOriginalName();
            // عم يعطيني اسم الصورة بدون امتداد وعم يدمجها مع الوقت مشان مايصير في تكرار
            $photo->move('uploads/stores', $newphoto);
        }

        $store->update($request->except('photo') + [
            'photo' => 'uploads/stores/' . $newphoto
                         ]);

        $store->save();


        $store->tags()->sync($request->tags); // مشان يعدل كل التاغات ويخزنها

        return redirect()
        ->route('store.index')
        ->with('success', 'edit store  successfully');

    }

    public function destroy($id)
    {
        store::find($id)->delete();
        return redirect()->route('store.index')->with('success', 'Store delete successfully.');
    }

}

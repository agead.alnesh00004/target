<?php

namespace App\Http\Controllers;

use App\Models\size;
use Illuminate\Http\Request;

class sizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = size::orderBy('id')->paginate(5);


        return view('size.index', compact('sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sizes = size::all();
        return view('size.create')->with('sizes', $sizes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:sizes'
        ]);


        size::create($request->all());

        return redirect()
        ->route('size.index')
        ->with('success', 'size create successfully.');
    }


    public function show($id)
    {
        $size = size::find($id);

        return view('size.show', compact('size'));
    }

    public function edit($id)
    {
        $size = size::find($id);
        return view('size.edit', compact('size'));
    }


    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $size = size::find($id);
        $size->name = $request->input('name');
        $size->save();

        return redirect()->route('size.index')
            ->with('success', 'size updated successfully.');
    }



    public function destroy($id)
    {
        size::find($id)->delete();

        return redirect()->route('size.index')
            ->with('success', 'size deleted successfully');
    }
}

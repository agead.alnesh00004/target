<?php

namespace App\Http\Controllers;

use App\Models\color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = color::orderBy('id')->paginate(5);

        return view('color.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('color.create');
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:colors'
        ]);


        color::create($request->all());

        return redirect()
        ->route('color.index')
        ->with('success', 'color create successfully.');
    }


    public function show($id)
    {
        $color = color::find($id);

        return view('color.show', compact('color'));
    }

    public function edit($id)
    {
        $color = color::find($id);
        return view('color.edit', compact('color'));
    }


    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $color = color::find($id);
        $color->name = $request->input('name');
        $color->save();

        return redirect()->route('color.index')
            ->with('success', 'color updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        color::find($id)->delete();

        return redirect()->route('color.index')
            ->with('success', 'color deleted successfully');
    }
}

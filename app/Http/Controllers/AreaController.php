<?php

namespace App\Http\Controllers;

use App\Models\area;
use App\Models\province;
use Illuminate\Http\Request;

class AreaController extends Controller
{


    public function index()
    {

     $areas = area::orderBy('id')->paginate(5);

        return view('area.index', compact('areas'));
    }


    public function create()
    {

        $provinces = province::all();

        if($provinces->count() == 0)
{
return redirect()->route('province.create');
}


        return view('area\create')
        ->with('provinces', $provinces);

    }


    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:areas',
            'province_id' => 'required'
        ]);

         area::create($request->all());

        return redirect()
        ->route('area.index')
        ->with('success', 'area add successfully');
    }



    public function show($id)
    {
        $area = area::find($id);

        return view('area.show', compact('area'));
    }


    public function edit($id)
    {

        $provinces = province::all();
        $area = area::find($id);
        return view('area.edit')
        ->with('provinces', $provinces) 
        ->with('area', $area);


        }


    public function update(Request $request,$id)
    {
        $area = area::find($id);
        $this->validate($request, [
            'name' => 'required|unique:areas',
            'province_id' => 'required'
        ]);


        $area->update($request->all());


        return redirect()
        ->route('area.index')
        ->with('success', 'area Update successfully');

    }


    public function destroy($id)
    {
        area::find($id)->delete();

        return redirect()->route('area.index')
            ->with('success', 'area deleted successfully');
    }
}


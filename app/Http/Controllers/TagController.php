<?php

namespace App\Http\Controllers;

use App\Models\tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = tag::orderBy('id')->paginate(5);

        return view('tag.index', compact('tags'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:tags'
        ]);


        tag::create($request->all());

        return redirect()
        ->route('tag.index')
        ->with('success', 'tag create successfully.');

    }


    public function show($id)
    {
        $tag = tag::find($id);

        return view('tag.show', compact('tag'));
    }



    public function edit($id)
    {
        $tag = tag::find($id);
        return view('tag.edit', compact('tag'));
    }



    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $tag = tag::find($id);
        $tag->name = $request->input('name');
        $tag->save();

        return redirect()->route('tag.index')
            ->with('success', 'tag updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        tag::find($id)->delete();

        return redirect()->route('tag.index')
            ->with('success', 'tag deleted successfully');
    }
}

<?php

namespace Database\Seeders;

use Hash;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Aghyad Albesh',
            'email' => 'agead.alnesh00004@gmail.com',
            'password' => Hash::make('00100101')
        ]);

        $store = User::create([
            'name' => 'zara',
            'email' => 'zara@gmail.com',
            'password' => Hash::make('00100101z')
        ]);


        $role = Role::find(1);
        $roleStore = Role::find(2);


        $permissions = Permission::pluck('id', 'id')->all();

        $role->syncPermissions($permissions);
        $roleStore->syncPermissions($permissions);


        $user->assignRole([$role->id]);
        $store->assignRole([$roleStore->id]);


        User::factory(14)->create();

    }
}

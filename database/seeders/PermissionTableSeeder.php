<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',

            'role-list',
            'role-create',
            'role-edit',
            'role-delete',

            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',



            'province-list',
            'province-create',
            'province-edit',
            'province-delete',

            'area-list',
            'area-create',
            'area-edit',
            'area-delete',

            'tag-list',
            'tag-create',
            'tag-edit',
            'tag-delete',

            'subtag-list',
            'subtag-create',
            'subtag-edit',
            'subtag-delete',

            'store-list',
            'store-create',
            'store-edit',
            'store-delete',

            'color-list',
            'color-create',
            'color-edit',
            'color-delete',

            'size-list',
            'size-create',
            'size-edit',
            'size-delete',


            'product-list',
            'product-create',
            'product-edit',
            'product-delete',

            'add-to-cart',
            'view-cart'


        ];

        foreach ($data as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}

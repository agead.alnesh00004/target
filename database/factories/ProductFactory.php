<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'description'=>$this->faker->text(100),
            'price'=>$this->faker->randomElement(['100$', '200$','300$','400$']),
            'store_id'=>rand(1,10),
            'created_by_user_id'=>rand(1,10),

            'subtag_id'=>$this->faker->boolean ? rand(1,10) : null,

        ];
    }
}
